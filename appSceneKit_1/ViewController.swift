//
//  ViewController.swift
//  appSceneKit_1
//
//  Created by Guest User on 11/12/19.
//  Copyright © 2019 Guest User. All rights reserved.
//

import UIKit
import SceneKit


class ViewController: UIViewController {

    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var sliderAncho: UISlider!
    @IBOutlet weak var sliderAlto: UISlider!
    @IBOutlet weak var sliderLargo: UISlider!
    
    
    @IBOutlet weak var sliderCamaraX: UISlider!
    @IBOutlet weak var sliderCamaraY: UISlider!
    @IBOutlet weak var sliderCamaraZ: UISlider!
    
    @IBOutlet weak var sliderLuzX: UISlider!
    @IBOutlet weak var sliderLuzY: UISlider!
    @IBOutlet weak var sliderLuzZ: UISlider!
    
    @IBOutlet weak var controlFiguras: UISegmentedControl!
    
    
    
    var ancho = 1.0;
    var alto = 1.0;
    var largo = 1.0;
    
    var camara_x = Float(3.0)
    var camara_y = Float(3.0)
    var camara_z = Float(3.0)
    
    var luz_x = Float(1.5)
    var luz_y = Float(1.5)
    var luz_z = Float(1.5)
    
    /*
        0 = Cube
        1 = Sphere
     */
    var figure = 0
    var dibujo = SCNGeometry()
    
    var red = CGFloat(0.3)
    var green = CGFloat(0.5)
    var blue = CGFloat(0.4)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Crear y establecer vista
        let scenView = SCNView(frame: self.imageview.frame)
        scenView.backgroundColor = UIColor.white
        
        self.imageview.addSubview(scenView)
        
        let scene =  SCNScene()
        scenView.scene = scene
        //DEFINIR LA CAMARA
        let camara = SCNCamera()
        let camaraNode = SCNNode()
        camaraNode.camera = camara
        
        // Modifcar camara
        camaraNode.position = SCNVector3(x: camara_x, y:camara_y, z:camara_z)
        
        //DEFINIR LA LUZ
        let light = SCNLight()
        light.type = SCNLight.LightType.omni
        let lightNode = SCNNode()
        lightNode.light = light
        lightNode.position = SCNVector3(x:luz_x, y:luz_y, z:luz_z)
        
        //******* Inicialización
        /*var imageMaterial = SCNMaterial()
        imageMaterial.isDoubleSided = false
        imageMaterial.diffuse.contents = UIImage(named: "pattern1")
        */
        
        var color = UIColor(displayP3Red: self.red, green: self.green, blue: self.blue, alpha: 1)
        
        let material_color = SCNMaterial()
        material_color.diffuse.contents = color
                
        //******* Datos comunes
        
        
        let figuraNodo = SCNNode(geometry: dibujo)
        
        figuraNodo.geometry?.materials = [material_color]
        
        
        // Constraints
        let constraint = SCNLookAtConstraint(target: figuraNodo)
        
        constraint.isGimbalLockEnabled = true
        camaraNode.constraints = [constraint]
        
        scene.rootNode.addChildNode(lightNode)
        scene.rootNode.addChildNode(camaraNode)
        scene.rootNode.addChildNode(figuraNodo)
        
        scene.rootNode
        
    }

    @IBAction func sliderAnchoValueChanged(_ sender: UISlider) {
        self.ancho = Double(sender.value)
        updateDrawings()
    }
    
    @IBAction func sliderAltoValueChanged(_ sender: UISlider) {
        self.alto = Double(sender.value)
        updateDrawings()
        //self.setNeedsDisplay()
    }
    
    @IBAction func sliderLargoValueChanged(_ sender: UISlider) {
        self.largo = Double(sender.value)
        updateDrawings()
    }
    
    @IBAction func sliderCamaraXChanged(_ sender: UISlider) {
        self.camara_x = sender.value
        updateDrawings()
    }
    
    @IBAction func sliderCamaraYChanged(_ sender: UISlider) {
        self.camara_y = sender.value
        updateDrawings()
    }
    
    @IBAction func sliderCamaraZChanged(_ sender: UISlider) {
        self.camara_z = sender.value
        updateDrawings()
    }
    
    @IBAction func sliderLuzXChanged(_ sender: UISlider) {
        self.luz_x = sender.value
        updateDrawings()
    }
    
    @IBAction func sliderLuzYChanged(_ sender: UISlider) {
        self.luz_y = sender.value
        updateDrawings()
    }
    
    @IBAction func sliderLuzZChanged(_ sender: UISlider) {
        self.luz_z = sender.value
        updateDrawings()
    }
    
    @IBAction func controlFigurasChanged(_ sender: UISegmentedControl) {
        figure = sender.selectedSegmentIndex
        updateDrawings()
    }
    
    func updateDrawings(){
        switch figure {
        case 1:
            updateCircle()
            break;
        case 2:
            updateSphere()
            break;
        case 3:
            updatePyramid()
            break;
        case 4:
            updateCapsule()
            break;
        case 5:
            updateCono()
            break;
        default:
            break;
        }
        
        self.viewDidLoad()
    }
    
    func updateCircle(){
        dibujo = SCNBox(width: CGFloat(ancho), height: CGFloat(alto), length: CGFloat(largo), chamferRadius: 0.0)
    }
    
    func updateSphere(){
        dibujo = SCNSphere(radius: CGFloat(ancho))
    }
    
    func updatePyramid(){
        dibujo = SCNPyramid(width: CGFloat(ancho), height: CGFloat(alto), length: CGFloat(largo))
    }
    
    func updateCapsule(){
        dibujo = SCNCapsule(capRadius: CGFloat(ancho), height: CGFloat(largo))
    }
    
    func updateCono(){
        dibujo = SCNCone(topRadius: CGFloat(ancho), bottomRadius: CGFloat(largo), height: CGFloat(alto))
    }
    
    @IBAction func sliderRedChanged(_ sender: UISlider) {
        self.red = CGFloat(sender.value/10)
        updateDrawings()
    }
    
    @IBAction func sliderGreenChanged(_ sender: UISlider) {
        self.green = CGFloat(sender.value/10)
        updateDrawings()
    }
    
    @IBAction func sliderBlueChanged(_ sender: UISlider) {
        self.blue = CGFloat(sender.value/10)
        updateDrawings()
    }
    
}

